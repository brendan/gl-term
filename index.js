
module.exports.onWindow = browserWindow =>
browserWindow.setVibrancy("ultra-dark");

const foregroundColor = "#FFFAF4";
const backgroundColor = "rgba(0, 0, 0, .65)";
const overlap = "rgba(0, 0, 0, .15)";
const red = "#DB3B21";
const green = "#1AAA55";
const yellow = "#FDBC60";
const blue = "#1F78D1";
const magenta = "#5C35AE";
const cyan = "#73AFEA";
const white = "#FFFAF4";

const defaultConfig = {
    fontFamily:
      '"SF Mono", "Monaco", "Inconsolata", "Fira Mono", "Droid Sans Mono", "Source Code Pro", monospace',
    fontSize: 12,
  };


exports.decorateConfig = (config) => {
    return Object.assign({}, config, {
        backgroundColor,
        foregroundColor,
        borderColor: overlap,
        cursorColor: blue,
        colors: {
          black: backgroundColor,
          red,
          green,
          yellow,
          blue,
          magenta,
          cyan,
          white,
          lightBlack: "#707070",
          lightRed: red,
          lightGreen: green,
          lightYellow: yellow,
          lightBlue: blue,
          lightMagenta: magenta,
          lightCyan: cyan,
          lightWhite: foregroundColor,
        },
      css: `
        ${config.css || ''}
        .tabs_nav .tabs_list .tab_text {
          color: #FC9403;
        }
        .tabs_nav .tabs_title {
          color: #FC9403;
        }
      `
    });
  }